﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardButton : MonoBehaviour
{
    public CardData config;
    public Text card_txt;
    public Text selection_txt;
    public Image card_icon;
    public bool isActive;

    public void Start()
    {
        selection_txt.text = "";
    }
    public void OnSelection()
    {
        if (ModuleGame.instance.free_change)
        {
            if (!isActive)
            {
                selection_txt.text = "Оставить";
                isActive = true;
                ModuleGame.instance.AddSelectedCard(this);
            }
            else
            {
                isActive = false;
                selection_txt.text = null;

                List<CardButton> newList = new List<CardButton>();

                foreach (var card in ModuleGame.instance.selectedCards)
                {
                    if (card != this)
                    {
                        newList.Add(card);
                    }
                }
                ModuleGame.instance.selectedCards = newList;
                
            }
        }
    }

    public void SetDefaultIcon()
    {
        card_icon.sprite = ModuleGame.instance.default_icon;
    }
}
