﻿using UnityEngine;
using UnityEngine.UI;

public class WinTableString:MonoBehaviour
{
    public Text combination_txt;
    public Text reward_txt;
    public Image background;
    public int win_value;

    public Color default_txt_color;
    public Color selected_txt_color;

    public Color default_background_color;
    public Color selected_background_color;

    public void SetDefaultColor()
    {
        background.color = default_background_color;
        combination_txt.color = default_txt_color;
        reward_txt.color = default_txt_color;
    }

    public void SetWinColor()
    {
        background.color = selected_background_color;
        combination_txt.color = selected_txt_color;
        reward_txt.color = selected_txt_color;
    }
    public void ChangeWinValue(int value)
    {
        win_value = value;
        reward_txt.text = win_value + "$";
    }
}
