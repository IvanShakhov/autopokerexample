﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModuleGame : MonoBehaviour
{
    static public ModuleGame instance;
    public List<WinTableString> all_tables;
    public List<CardData> all_cards;
    public List<CardButton> hand_cards;
    public List<CardButton> selectedCards;
    public List<CardData> swap_cards;

    public List<int> pair_values;
    public List<int> all_values;

    public Text balance_txt;
    public Text bet_txt;
    public Text win_txt;

    public Sprite default_icon;

    public int balance;
    public int bet;
    public int reward;

    [SerializeField]
    private AllPokerCombinations combinations = new AllPokerCombinations();

    public GameObject button_swap_cards;
    public GameObject button_start_round;
    public GameObject button_get_reward;

    public bool free_change = false;
    //public List<>
    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        RefreshTables();
        button_start_round.SetActive(true);
        button_swap_cards.SetActive(false);
        button_get_reward.SetActive(false);
    }

    public void AddSelectedCard(CardButton data)
    {
        selectedCards.Add(data);
        if (free_change)
        {
            button_swap_cards.SetActive(true);
            button_get_reward.SetActive(false);
        }
    }

    public void StartRound()
    {
        

        if (bet <= 0)
        {
            win_txt.text = "Ставка слишком мала, увеличте";
            return;
        }

        if (balance >= bet)
        {
            balance -= bet;
            balance_txt.text = balance + "$";
            win_txt.text = "";
            button_swap_cards.SetActive(true);
            button_start_round.SetActive(false);
            free_change = true;
            SwapCards(false);
           
        }
        else
        {
            win_txt.text = "Пополните ваш баланс! Или уменьшите ставку!";
        }
    }

    public void SwapCards(bool change)
    {
        RefreshTables();
        if (change)
        {
            free_change = false;
        }
        foreach(var handcard in hand_cards)
        {
            var temp = selectedCards.Find(x => x.config == handcard.config);
            if (temp == null)
            {
                while (true)
                {
                    CardData data = GetRandomCard();
                    var check1 = hand_cards.Find(x => x.config == data);
                    var check2 = swap_cards.Find(x => x == data);
                    if (check1 == null && check2 == null)
                    {
                        handcard.config = data;
                        //handcard.card_txt.text = data.rank + data.suit.ToString();
                        handcard.card_txt.text = "";
                        handcard.card_icon.sprite = data.icon;
                        break;
                    }
                }
            }
            handcard.selection_txt.text = "";
            handcard.isActive = false;
            
        }
        
        PokerLogic();
        //button_swap_cards.SetActive(false);
        selectedCards = new List<CardButton>();
    }


    public void PokerLogic()
    {
        combinations.Clear();

        foreach(var num in pair_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (pair.Count == 2)
            {
                combinations.isPair = true;
                combinations.max_combination = "isPair";
                break;
            }
        }

        bool isFirstPair = false;
        foreach (var num in all_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (!isFirstPair)
            {
                if (pair.Count == 2)
                {
                    isFirstPair = true;
                }
            }
            else
            {
                if (pair.Count == 2)
                {
                    combinations.isTwoPair = true;
                    combinations.max_combination = "isTwoPair";
                    isFirstPair = false;
                    break;
                }
            }
        }

        foreach (var num in all_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (pair.Count == 3)
            {
                combinations.isTwoPair = true;
                combinations.max_combination = "isThree";
                isFirstPair = false;
                break;
            }
        }

        List<int> straight = new List<int>();
        bool isStraich = true;
        foreach (var num in all_values)
        {
            var card = hand_cards.FindAll(x => x.config.rank == num);
            if (card.Count>1)
            {
                isStraich = false;
                break;
            }
            else
            {
                if (card.Count == 1)
                {
                    straight.Add(num);
                }
            }
        }
        if (isStraich)
            for (int i = 0; i < straight.Count; i++)
            {
                if (i + 1 < straight.Count)
                {
                    if (straight[i + 1] - straight[i] == 1)
                    {
                        if (i + 1 == straight.Count)
                        {
                            combinations.isStraight = true;
                            combinations.max_combination = "isStraight";
                            break;
                        }
                    }
                    else
                    {
                        break;
                    }
                }
            }

        for (int i = 0; i < hand_cards.Count; i++)
        {
            if (hand_cards[0].config.suit == hand_cards[i].config.suit)
            {
                if (i + 1 == hand_cards.Count)
                {
                    combinations.isFlush = true;
                    combinations.max_combination = "isFlush";
                    break;
                }
            }
            else
            {
                break;
            }
        }

        bool house_first = false;
        foreach (var num in all_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (!house_first)
            {
                if (pair.Count == 3)
                {
                    house_first = true;
                }
            }
            
        }
        foreach (var num in all_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (house_first)
            {
                if (pair.Count == 2)
                {
                    combinations.isFullHouse = true;
                    combinations.max_combination = "isFullHouse";
                    break;
                }
            }
            else { break; }

        }

        foreach (var num in all_values)
        {
            var pair = hand_cards.FindAll(x => x.config.rank == num);
            if (!house_first)
            {
                if (pair.Count == 4)
                {
                    combinations.isFour = true;
                    combinations.max_combination = "isFour";
                    break;
                }
            }

        }

        if (combinations.isStraight && combinations.isFlush)
        {
            var card = hand_cards.Find(x => x.config.rank == all_values[all_values.Count-1]);
            if (card != null)
            {
                combinations.isRoyalFlush = true;
                combinations.max_combination = "isRoyalFlush";
            }
            else
            {
                combinations.isStraightFlush = true;
                combinations.max_combination = "isStraightFlush";
            }
        }

        switch (combinations.max_combination)
        {
            default:
                {
                    if (!free_change)
                    {
                        win_txt.text = "Вы ничего не выиграли";
                        reward = 0;
                        button_start_round.SetActive(true);
                        button_swap_cards.SetActive(false);
                        button_get_reward.SetActive(false);
                        RefreshTables();


                    }
                    else
                    {
                        win_txt.text = "Выберите карты которые хотите оставить!";
                        button_swap_cards.SetActive(true);
                    }
                }
                break;
            case "isRoyalFlush":
                {
                    SelectElementInWinTable(0);
                }
                break;
            case "isStraightFlush":
                {
                    SelectElementInWinTable(1);
                }
                break;
            case "isFour":
                {
                    SelectElementInWinTable(2);
                }
                break;
            case "isFullHouse":
                {
                    SelectElementInWinTable(3);
                }
                break;
            case "isFlush":
                {
                    SelectElementInWinTable(4);
                }
                break;
            case "isStraight":
                {
                    SelectElementInWinTable(5);
                }
                break;
            case "isThree":
                {
                    SelectElementInWinTable(6);
                }
                break;
            case "isTwoPair":
                {
                    SelectElementInWinTable(7);
                }
                break;
            case "isPair":
                {
                    SelectElementInWinTable(8);
                }
                break;
        }

    }

    public void SelectElementInWinTable(int index)
    {
        all_tables[index].SetWinColor();
        win_txt.text = "Ваш выигрыш " + all_tables[index].reward_txt.text;
        reward = all_tables[index].win_value;
        button_get_reward.SetActive(true);
    }

    public void RefreshTables()
    {
        foreach(var table in all_tables)
        {
            table.SetDefaultColor();
        }
    }

    public CardData GetRandomCard()
    {
        return all_cards[Random.Range(0, all_cards.Count)];
    }

    public void GetReward()
    {
        balance += reward;
        balance_txt.text = balance + "$";
        reward = 0;
        win_txt.text = "";
        RefreshTables();
        button_get_reward.SetActive(false);
        button_start_round.SetActive(true);
        foreach(CardButton card in hand_cards)
        {
            card.SetDefaultIcon();
        }
    }

    public void ChangeBet(int value)
    {
        
    }
}

[System.Serializable]
public sealed class AllPokerCombinations
{
    public string max_combination;
    public bool isPair;
    public bool isTwoPair;
    public bool isThee;
    public bool isStraight;
    public bool isFlush;
    public bool isFullHouse;
    public bool isFour;
    public bool isStraightFlush;
    public bool isRoyalFlush;

    public void Clear()
    {
        isPair = false;
        isTwoPair = false;
        isThee = false;
        isStraight = false;
        isFlush = false;
        isFullHouse = false;
        isFour = false;
        isStraightFlush = false;
        isRoyalFlush = false;
        max_combination = "";
    }
}
