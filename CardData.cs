﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "config_card_", menuName = "config_card", order = 1)]
public class CardData : ScriptableObject
{
    public int rank;
    public ENUM_CARD_SUIT suit;
    public Sprite icon;
}

public enum ENUM_CARD_SUIT
{
    DIAMONDS,HEARTS,CLUBS,SPADES
}

